import smtplib
import messenger
from urllib2 import unquote

class RCMessenger(messenger.Messenger):
    def __init__(self, api_token, chat_id):
        messenger.Messenger.__init__(self, api_token, chat_id)
        self.__host = "smtp.gmail.com"
        self.__port = 587
        self.__user = "juan.menendez@rockwellcollins.com"
        self.__pw   = "zojviriotudhounr"

    def send_mail_notification(self, subj, msg):
        mailer = smtplib.SMTP(self.__host, self.__port)
        mailer.starttls()
        mailer.ehlo()
        mailer.login(self.__user, self.__pw)
        mailer.sendmail("juan.menendez@rockwellcollins.com",
                        "juan.menendez@rockwellcollins.com",
                        "Subject:RC Notifier - {}\n{}".format(subj, msg))
        mailer.quit()

    def parse_message(self, env):
        s = env['QUERY_STRING'].split("!")
        assert len(s) == 2, "Invalid message format"
        return "*" + unquote(s[0]) + "* -> " + unquote(s[1])
