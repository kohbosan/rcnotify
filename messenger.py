from urllib2 import urlopen, URLError
from urllib import urlencode
from json import loads, dumps


class Messenger:
    def __init__(self, api_token, chat_id):
        self.api_token = api_token
        self.url = "https://api.telegram.org/" + api_token
        self.chat_id = chat_id

    def parse_message(self, env):
        print(repr(env))
        return repr(env)

    def handle_request(self, env):
        data = {
            "chat_id": self.chat_id,
            "text": self.parse_message(env),
            "parse_mode": "Markdown"
        }
        try:
            response = urlopen(self.url + "/sendMessage", data=urlencode(data)).read()
            assert loads(response)['ok'] == True, response
        except URLError:
            print("Something went wrong with the connection.")
        except AssertionError:
            print("Something went wrong while sending message.")
        return bytes(dumps(data))

    def make_link(self, text, url):
        return "[" + text + "](" + url + ")"
